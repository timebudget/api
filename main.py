from datetime import date, timedelta
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import List


app = FastAPI()

# origins = [
#     "http://localhost",
#     "http://localhost:8080",
#     "http://timebudget-api",
#     "http://timebudget"
# ]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Category(BaseModel):
    name: str
    time: int
    logged: int = 0

categories = {
        1: Category(name='Programming', time=timedelta(hours=8).total_seconds()),
        2: Category(name='Guitar', time=timedelta(hours=2).total_seconds()),
        }
last_id = 2

@app.get('/categories/{category_id}')
async def category(category_id: int):
    return { 'id': category_id, 'object': categories[category_id] }

@app.get('/categories')
async def list_categories():
    return [ { 'id': x, 'object': y } for x,y in categories.items() ]

@app.post('/categories') 
async def create_category(new_category: Category):
    global last_id
    last_id += 1
    categories[last_id] = new_category
    return { 'id': last_id, 'object': categories[last_id] }

@app.delete('/categories/{category_id}')
async def delete_category(category_id: int):
    del categories[category_id]

@app.put('/categories/{category_id}')
async def update_category(category_id: int, category: Category):
    categories[category_id] = category

@app.post('/categories/{category_id}/log')
async def log_time(category_id: int, duration: int):
    categories[category_id].logged += duration
    return { 'id': category_id, 'object': categories[category_id] }

@app.delete('/categories/{category_id}/log')
async def clear_time(category_id: int):
    categories[category_id].logged = 0
    return { 'id': category_id, 'object': categories[category_id] }

