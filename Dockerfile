FROM tiangolo/uvicorn-gunicorn:python3.8-alpine3.10

ENV MAX_WORKERS 1
ENV WEB_CONCURRENCY 1
WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY main.py .
